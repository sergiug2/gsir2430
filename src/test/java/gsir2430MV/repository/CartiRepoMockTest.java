package gsir2430MV.repository;

import gsir2430MV.controller.BibliotecaCtrl;
import gsir2430MV.model.Carte;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CartiRepoMockTest {

    private CartiRepoMock repo = new CartiRepoMock();
    private BibliotecaCtrl controller = new BibliotecaCtrl(repo);

    @Test
    public void TC01_EC() {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        Integer repo_size = null;
        repo_size = this.controller.getCarti().size();


        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
        } catch (Exception e) {
            assertTrue(false);
        }

        assert (this.controller.getCarti().size() == repo_size + 1);
        assertTrue(true);
    }

    @Test
    public void TC02_EC(){
        String carte = "1200;Andy Weir;2010;Crown;pa,pu";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC03_EC(){
        String carte = "The Martian;;2010;Crown;pa,pu";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC08_EC(){
        String carte = "The Martian;Andy Weir;2010;Crown;";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC03_BVA(){
        String carte = "The Martian;Andy Weir;2010;Crown;unu";

        Integer repo_size = null;
        repo_size = this.controller.getCarti().size();

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
        } catch (Exception e) {
            assertTrue(false);
        }

        assert (this.controller.getCarti().size() == repo_size + 1);
        assertTrue(true);
    }

    @Test
    public void TC06_BVA(){
        String carte = "The Martian;Andy Weir;2010;Crown;";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            assertTrue(false);
        } catch (Exception e) {
            assertTrue(true);
        }
    }

    @Test
    public void TC15_BVA(){
        String carte = "The Martian;Andy Weir;2010A;Crown;unu,doi";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            assertTrue(false);

        } catch (Exception e) {
            assertTrue(true);
        }
    }
    @Test
    public void TC12_BVA(){
        String carte = "The Martian;Andy Weir;ieri;Crown;unu,doi";

        try {
            this.controller.adaugaCarte(Carte.getCarteFromString(carte));
            if (this.controller.cautaCarte("Andy Weir").size() > 0) {
                assertTrue(true);
            }
            assertTrue(false);

        } catch (Exception e) {
            assertTrue(true);
        }
    }
}
