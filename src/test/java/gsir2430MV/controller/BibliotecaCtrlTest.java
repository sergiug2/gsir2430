package gsir2430MV.controller;

import gsir2430MV.model.Carte;
import gsir2430MV.repository.CartiRepoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BibliotecaCtrlTest {

    private BibliotecaCtrl bibliotecaCTRL;

    @Before
    public void setUp() throws Exception {
        this.bibliotecaCTRL = new BibliotecaCtrl(new CartiRepoMock());
    }

    @Test
    public void TC_1() throws Exception {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        this.bibliotecaCTRL.adaugaCarte(Carte.getCarteFromString(carte));
    }

    @Test(expected = Exception.class)
    public void TC_2() throws Exception {
        String carte = "The Martian;Andy Weir;aaa;Crown;unu,doi";
        this.bibliotecaCTRL.adaugaCarte(Carte.getCarteFromString(carte));
    }
}