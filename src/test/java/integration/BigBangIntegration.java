package integration;

import gsir2430MV.controller.BibliotecaCtrl;
import gsir2430MV.model.Carte;
import gsir2430MV.repository.CartiRepoMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class BigBangIntegration {
    private CartiRepoMock cartiRepoMock;
    private BibliotecaCtrl bibliotecaCtrl;

    @Before
    public void setup() {
        cartiRepoMock = new CartiRepoMock();
        bibliotecaCtrl = new BibliotecaCtrl(cartiRepoMock);
    }

    @Test
    public void addBookTest() {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        this.cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte));
        Assert.assertEquals(cartiRepoMock.getCarti().size(), 1);
        Assert.assertEquals(cartiRepoMock.getCarti().get(0).getTitlu(), "The Martian");
    }

    // F02 unit test
    @Test
    public void findBookSuccessTest() throws Exception {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte));
        List<Carte> result = cartiRepoMock.cautaCarte("Andy Weir");
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0).getTitlu(), "The Martian");
    }

    // F03 unit test
    @Test
    public void testSortingByTitle() throws Exception {
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        String carte2 = "Harry Potter;Rowling;2010;Corint;unu,doi";

        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte1));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte2));

        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul("2010");

        Assert.assertEquals(result.get(0).getTitlu(), "Harry Potter");
        Assert.assertEquals(result.get(1).getTitlu(), "The Martian");
    }

    // Integration test
    @Test
    public void integrationTest() throws Exception {
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        String carte2 = "Harry Potter;Rowling;2011;Corint;unu,doi";
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte1));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte2));

        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").size(), 1);
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").get(0).getTitlu(), "The Martian");
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Rowling").size(), 1);

        Assert.assertEquals(bibliotecaCtrl.getCartiOrdonateDinAnul("2010").get(0).getTitlu(), "The Martian");
        Assert.assertEquals(bibliotecaCtrl.getCartiOrdonateDinAnul("2011").get(0).getTitlu(), "Harry Potter");
    }
}
