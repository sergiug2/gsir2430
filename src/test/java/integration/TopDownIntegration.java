package integration;

import gsir2430MV.controller.BibliotecaCtrl;
import gsir2430MV.model.Carte;
import gsir2430MV.repository.CartiRepo;
import gsir2430MV.repository.CartiRepoMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TopDownIntegration {

    private CartiRepoMock cartiRepoMock;
    private BibliotecaCtrl bibliotecaCtrl;

    @Before
    public void setup() {
        cartiRepoMock = new CartiRepoMock();
        bibliotecaCtrl = new BibliotecaCtrl(cartiRepoMock);
    }

    @Test
    public void addBookTest() {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        this.cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte));
        Assert.assertEquals(cartiRepoMock.getCarti().size(), 1);
        Assert.assertEquals(cartiRepoMock.getCarti().get(0).getTitlu(), "The Martian");
    }

    @Test
    public void findBookSuccessTest() {
        String carte = "The Martian;Andy Weir;2010;Crown;unu,doi";
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte));
        List<Carte> result = cartiRepoMock.cautaCarte("Andy Weir");
        Assert.assertEquals(result.size(), 1);
        Assert.assertEquals(result.get(0).getTitlu(), "The Martian");
    }

    @Test
    public void testSortingByTitle() {
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        String carte2 = "Harry Potter;Rowling;2010;Corint;unu,doi";

        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte1));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte2));

        List<Carte> result = cartiRepoMock.getCartiOrdonateDinAnul("2010");

        Assert.assertEquals(result.get(0).getTitlu(), "Harry Potter");
        Assert.assertEquals(result.get(1).getTitlu(), "The Martian");
    }

    @Test
    public void addBookIntegrationTest() throws Exception {
        Assert.assertEquals(bibliotecaCtrl.getCarti().size(), 0);
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        bibliotecaCtrl.adaugaCarte(Carte.getCarteFromString(carte1));
        Assert.assertEquals(bibliotecaCtrl.getCarti().size(), 1);
        Assert.assertEquals(bibliotecaCtrl.getCarti().get(0).getTitlu(), "The Martian");
        String carte2 = "Harry Potter;Rowling;2010;Corint;unu,doi";
        bibliotecaCtrl.adaugaCarte(Carte.getCarteFromString(carte2));
        Assert.assertEquals(bibliotecaCtrl.getCarti().size(), 2);
        Assert.assertEquals(cartiRepoMock.getCarti().get(1).getTitlu(), "Harry Potter");
    }

    @Test
    public void findBookIntegrationTest() throws Exception {
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        String carte2 = "Harry Potter;Rowling;2010;Corint;unu,doi";
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte1));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte2));
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").size(), 1);
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").get(0).getTitlu(), "The Martian");
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Rowling").size(), 1);
    }

    @Test
    public void integrationTest() throws Exception {
        String carte1 = "The Martian;Andy Weir;2010;Crown;unu,doi";
        String carte2 = "Harry Potter;Rowling;2011;Corint;unu,doi";
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte1));
        cartiRepoMock.adaugaCarte(Carte.getCarteFromString(carte2));

        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").size(), 1);
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Andy Weir").get(0).getTitlu(), "The Martian");
        Assert.assertEquals(bibliotecaCtrl.cautaCarte("Rowling").size(), 1);

        Assert.assertEquals(bibliotecaCtrl.getCartiOrdonateDinAnul("2010").get(0).getTitlu(), "The Martian");
        Assert.assertEquals(bibliotecaCtrl.getCartiOrdonateDinAnul("2011").get(0).getTitlu(), "Harry Potter");

    }
}
