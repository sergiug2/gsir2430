package gsir2430MV.begin;

import gsir2430MV.controller.BibliotecaCtrl;
import gsir2430MV.repository.CartiRepoInterface;
import gsir2430MV.repository.CartiRepoMock;
import gsir2430MV.view.Consola;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        CartiRepoInterface cr = new CartiRepoMock();
        BibliotecaCtrl bc = new BibliotecaCtrl(cr);
        Consola c = new Consola(bc);
        try {
            c.executa();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
