package gsir2430MV.controller;


import gsir2430MV.model.Carte;
import gsir2430MV.repository.CartiRepoInterface;
import gsir2430MV.utils.Validator;

import java.util.List;

public class BibliotecaCtrl {

	private CartiRepoInterface cr;
	
	public BibliotecaCtrl(CartiRepoInterface cr){
		this.cr = cr;
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		Validator.validateCarte(c);
		cr.adaugaCarte(c);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		Validator.isOKString(autor);
		return cr.cautaCarte(autor);
	}
	
	public List<Carte> getCarti() {
		return cr.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		if(!Validator.isNumber(an))
			throw new Exception("Nu e numar!");
		return cr.getCartiOrdonateDinAnul(an);
	}
	
	
}
