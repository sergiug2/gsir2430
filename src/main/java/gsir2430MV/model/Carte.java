package gsir2430MV.model;


import gsir2430MV.utils.Validator;

import java.util.ArrayList;
import java.util.List;

public class Carte {
	
	private String titlu;
	private List<String> referenti;
	private String anAparitie;
	private String editura;
	private List<String> cuvinteCheie;
	
	public Carte(){
		this.titlu = "";
		this.referenti = new ArrayList<>();
		this.anAparitie = "";
		this.editura = "";
		this.cuvinteCheie = new ArrayList<>();
	}

	public Carte(String titlu, List<String> referenti, String anAparitie, String editura, List<String> cuvinteCheie) {
		this.titlu = titlu;
		this.referenti = referenti;
		this.anAparitie = anAparitie;
		this.editura = editura;
		this.cuvinteCheie = cuvinteCheie;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	public void setReferenti(List<String> referenti) {
		this.referenti = referenti;
	}

	public void setAnAparitie(String anAparitie) {
		this.anAparitie = anAparitie;
	}

	public void setEditura(String editura) {
		this.editura = editura;
	}

	public void setCuvinteCheie(List<String> cuvinteCheie) { this.cuvinteCheie = cuvinteCheie; }

	public String getTitlu() {
		return titlu;
	}

	public List<String> getReferenti() {
		return referenti;
	}

	public String getEditura() {
		return editura;
	}

	public String getAnAparitie() {
		return anAparitie;
	}

	public List<String> getCuvinteCheie() {
		return cuvinteCheie;
	}

	public void adaugaCuvantCheie(String cuvant){
		cuvinteCheie.add(cuvant);
	}
	
	public void adaugaReferent(String ref){
		referenti.add(ref);
	}
	
	@Override
	public String toString(){
		String ref = "";
		String cuvCheie = "";
		
		for(int i=0;i<referenti.size();i++){
			if(i==referenti.size()-1)
				ref+=referenti.get(i);
			else
				ref+=referenti.get(i)+",";
		}
		
		for(int i=0;i<cuvinteCheie.size();i++){
			if(i==cuvinteCheie.size()-1)
				cuvCheie+=cuvinteCheie.get(i);
			else
				cuvCheie+=cuvinteCheie.get(i)+",";
		}
		
		return titlu+";"+ref+";"+anAparitie+";"+editura+";"+cuvCheie;
	}
	
	public static Carte getCarteFromString(String carte){
		Carte c = new Carte();
		String []atr = carte.split(";");
		c.titlu=atr[0];

		try {
			String []referenti = atr[1].split(",");
			for(String s:referenti){
				c.adaugaReferent(s);
			}
		} catch (Exception e) {
			c.referenti = new ArrayList<>();
		}

		c.anAparitie = atr[2];
		c.editura = atr[3];

		try {
			String []cuvCheie = atr[4].split(",");
			for(String s:cuvCheie){
				c.adaugaCuvantCheie(s);
			}
		}catch (Exception e) {
			c.cuvinteCheie = new ArrayList<>();
		}

		try {
			Validator.validateCarte(c);
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
		}

		return c;
	}
	
}
